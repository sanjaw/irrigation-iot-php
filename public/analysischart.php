<?php
	include('server.php');

	// Fetch values from database table 'devicedata'
	$query = "SELECT extract(EPOCH FROM time_added::TIMESTAMP WITHOUT TIME ZONE) as time_added, soil_moisture, humidity, temp FROM devicedata";
	$result = pg_query($db, $query);

	$data = pg_fetch_all($result);

	pg_close($db);

	// now print the data
	print json_encode($data);
?>