// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Analysis line chart
$(document).ready(function() {
	$.ajax({
		url: "analysischart.php",
		method: "GET",
		success: function(data) {
			data = JSON.parse(data);
			//console.log(data);
			
			var time_added = [];
			var soil_moisture = [];
			var humidity = [];
			var temperature = [];

			for(i in data) {
				time_added.push(new Date(data[i].time_added * 1000).toLocaleString());
				soil_moisture.push(data[i].soil_moisture);
				humidity.push(data[i].humidity);
				temperature.push(data[i].temp);
			}
			
			var chartdata = {
				labels: time_added,
				datasets: [{
					label: 'Soil Moisture',
					lineTension: 0.3,
					//backgroundColor: "rgba(2,117,216,0.2)",
					borderColor: "rgba(2,117,216,1)",
					pointRadius: 5,
					pointBackgroundColor: "rgba(2,117,216,1)",
					pointBorderColor: "rgba(255,255,255,0.8)",
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(2,117,216,1)",
					pointHitRadius: 50,
					pointBorderWidth: 2,
					data: soil_moisture
				},
				{
					label: 'Humidity',
					lineTension: 0.3,
					//backgroundColor: "rgba(59, 89, 152, 0.75)",
					borderColor: "rgba(59, 89, 152, 1)",
					pointRadius: 5,
					//pointBackgroundColor: "rgba(59, 89, 152, 1)",
					pointBorderColor: "rgba(59, 89, 152, 1)",
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
					pointHitRadius: 50,
					pointBorderWidth: 2,
					data: humidity
				}, 
				{
					label: 'Temperature',
					lineTension: 0.3,
					//backgroundColor: "rgba(211, 72, 54, 0.75)",
					borderColor: "rgba(211, 72, 54, 1)",
					pointRadius: 5,
					pointBackgroundColor: "rgba(2,117,216,1)",
					pointBorderColor: "rgba(255,255,255,0.8)",
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
					pointHoverBorderColor: "rgba(211, 72, 54, 1)",
					pointHitRadius: 50,
					pointBorderWidth: 2,
					data: temperature
				}]
			};

			var ctx = $('#myAnalysisChart');

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error: function(data) {
			//console.log("error" + data);
		}
	});
});