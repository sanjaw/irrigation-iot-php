// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Analysis line chart
$(document).ready(function() {
	$.ajax({
		url: "analysischart.php",
		method: "GET",
		success: function(data) {
			data = JSON.parse(data);			
			var humidity = [];
			var time_added = [];
			for(i in data) {
				time_added.push(new Date(data[i].time_added * 1000).toLocaleString());
				humidity.push(data[i].humidity);
			}

			var chartdata = {
				labels: time_added,
				datasets: [{
					label: 'Humidity',
					lineTension: 0.3,
					//backgroundColor: "rgba(59, 89, 152, 0.75)",
					borderColor: "rgba(59, 89, 152, 1)",
					pointRadius: 5,
					//pointBackgroundColor: "rgba(59, 89, 152, 1)",
					pointBorderColor: "rgba(59, 89, 152, 1)",
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
					pointHitRadius: 50,
					pointBorderWidth: 2,
					data: humidity
				}]
			};

			var ctx = $('#myHumidityChart');

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error: function(data) {
			//console.log("error" + data);
		}
	});
});