// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Analysis line chart
$(document).ready(function() {
	$.ajax({
		url: "analysischart.php",
		method: "GET",
		success: function(data) {
			data = JSON.parse(data);			
			var temperature = [];
			var time_added = [];
			for(i in data) {
				time_added.push(new Date(data[i].time_added * 1000).toLocaleString());
				temperature.push(data[i].temp);
			}

			var chartdata = {
				labels: time_added,
				datasets: [{
					label: 'Temperature',
					lineTension: 0.3,
		      		backgroundColor: "rgba(211, 72, 54, 0.75)",
					borderColor: "rgba(211, 72, 54, 1)",
					pointRadius: 5,
					pointBackgroundColor: "rgba(2,117,216,1)",
					pointBorderColor: "rgba(255,255,255,0.8)",
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(211, 72, 54, 1)",
					pointHoverBorderColor: "rgba(211, 72, 54, 1)",
					pointHitRadius: 50,
					pointBorderWidth: 2,
					data: temperature
				}]
			};

			var ctx = $('#myTemperatureChart');

			var lineGraph = new Chart(ctx, {
				type: 'line',
				data: chartdata
			});
		},
		error: function(data) {
			//console.log("error" + data);
		}
	});
});