<?php 
	include('server.php'); 
  if(!$_SESSION) {
    header('Location: login.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IrrIoT - Databases</title>

    <!-- Bootstrap core CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <!--<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1 font-weight-bold" href="index.php">IRRIOT</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php if($_SESSION): ?>
            <?=$_SESSION['username'] ?>
            <?php endif; ?>
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <!--<a class="dropdown-item" href="#">Settings</a>-->
            <!--<a class="dropdown-item" href="#">Activity Log</a>-->
            <!--<div class="dropdown-divider"></div>-->
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="devices.php">
            <i class="fas fa-fw fa-plug"></i>
            <span>Devices</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="analysis.php"> <!--charts.html-->
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Analysis</span></a>
        </li>        
        <li class="nav-item">
          <a class="nav-link" href="database.php">
            <i class="fas fa-fw fa-table"></i>
            <span>Database</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Database</a>
            </li>
            <li class="breadcrumb-item active"></li>
          </ol>

          <!-- Page Content -->
          <h4>Device Data</h4>
          <hr>
          <!-- TODO: Add Device selection -->

          <!-- Database 'devicedata' table -->
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                  <tr>
                    <th>Entry ID</th>
                    <th>Device ID</th>
                    <th>Time Added</th>
                    <th>Soil Moisture</th>
                    <th>Temperature</th>
                    <th>Humidity</th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                  $table_query = pg_query($db, "SELECT entryid, deviceid, time_added, soil_moisture, temp, humidity FROM devicedata");
                  while ($row = pg_fetch_assoc($table_query)) {
                    echo 
                      "<tr>
                        <td>". $row['entryid'] ."</td>
                        <td>". $row['deviceid'] ."</td>
                        <td>". $row['time_added'] ."</td>
                        <td>". $row['soil_moisture'] ."</td>
                        <td>". $row['temp'] ."</td>
                        <td>". $row['humidity'] ."</td>
                      </tr>";
                  }
                ?>
              </tbody>
            </table>
          </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Add Device Modal-->
    <div class="modal fade" id="addDeviceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Device</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          	<div class="modal-body">Enter the device details below.
	          	<form method="post" action="devices.php" id="addDeviceForm">
	        		<?php include('errors.php'); ?>
		            <div class="form-group">
		              <div class="form-label-group">
		                <input type="text" id="inputDeviceName" class="form-control" placeholder="Device Name" name="device_name" required="required">
		                <label for="inputDeviceName">Device Name</label>
		              </div>
		            </div>
                <div class="form-group">
                  <div class="form-label-group">
                    <input type="text" id="inputDeviceDesc" class="form-control" placeholder="Device ID" name="device_desc" required="required" autofocus="autofocus">
                    <label for="inputDeviceId">Device Description</label>
                  </div>
                </div>
	        	</form>
        	</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" form="addDeviceForm" name="add_device">Done</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    

    <!-- Core plugin JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

  </body>

</html>
