<?php 
	include('server.php');

	//print file_get_contents('php://input');
	if(!$_GET || (!$_GET['devicename'] || !$_GET['soil_moisture'])) die ("Invalid API syntax");

	$device_name = pg_escape_string($db, $_GET['devicename']);
	$soil_moisture = pg_escape_string($db, $_GET['soil_moisture']);
	$contents = file_get_contents('owmfile');
	$clima=json_decode($contents);
	$temp=$clima->main->temp;
	$humidity=$clima->main->humidity;


	if(empty($device_name)) { array_push($errors, "Empty devicename"); }
	if(empty($soil_moisture)) { array_push($errors, "Empty data"); }

	if(count($errors) == 0) {
		$current_time = date("Y-m-d H:i:s");
		$chk_device_name_query = "SELECT * FROM devices WHERE devicename='$device_name' LIMIT 1";
		$devices = pg_query($db, $chk_device_name_query);
		$device = pg_fetch_assoc($devices);

		// Insert the data into devicedata
		if($device) {
			$query = "INSERT INTO devicedata(deviceid, time_added, soil_moisture, temp, humidity) VALUES ((SELECT deviceid FROM devices WHERE devicename='$device_name' LIMIT 1), '$current_time', '$soil_moisture', '$temp', '$humidity')";
			$updateconn = "UPDATE devices SET lastconn='$current_time' WHERE deviceid='$deviceid'";
			$ins_result = pg_query($db, $query);
			$upd_result = pg_query($db, $query);
			if($ins_result && $upd_result)
				echo "Success";
			else 
				echo "Failed";
		} else {
			die("Device not available");
		}
	} else {
		die("API is incomplete");
	}

?>