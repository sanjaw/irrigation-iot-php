<?php 
  include('server.php');
  // Register User
  if(isset($_POST['reg_user'])) {
    // Receive all input variables from the form
    $username = pg_escape_string($db, $_POST['username']);
    $email = pg_escape_string($db, $_POST['email']);
    $password_1 = pg_escape_string($db, $_POST['password_1']);
    $password_2 = pg_escape_string($db, $_POST['password_2']);

    // form validation: ensure that form is correctly filled by adding
    // (array_push()) corresponding error into $errors array
    if(empty($username)) { array_push($errors, "Username is required"); }
    if(empty($email)) { array_push($errors, "Email is required"); }
    if(empty($password_1)) { array_push($errors, "Password is required"); }
    if(empty($password_2)) { array_push($errors, "Please confirm the Password"); }
    if($password_1 != $password_2) {
      array_push($errors, "The two passwords do not match");
    }

    // First check the database to make sure a user already exists with the same 
    // username and/or email
    $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
    $result = pg_query($db, $user_check_query);
    $user = pg_fetch_assoc($result);
    // check if user exists 
    if($user) {
      if($user['username'] === $username) {
        array_push($errors, "Username already exists");
      }
      if($user['email'] === $email) {
        array_push($errors, "Email already exists");
      }
    }

    // Register the user if there are no errors in the form
    if(count($errors) == 0) {
      $password = md5($password_1); // TODO: encrypt using crypt() blowfish algo for mor security. see password_hash()
      //echo $password;
      $current_time = date("Y-m-d H:i:s");
      $query = "INSERT INTO users(username, email, password, created_on) VALUES ('$username', '$email', '$password', '$current_time')";
      pg_query($db, $query);
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in";
      header('location: login.php');
    }
  }
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IrrIoT - Register</title>

    <!-- Bootstrap core CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <!--<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register an Account</div>
        <div class="card-body">
          <form method="post" action="register.php">
            <?php include('errors.php'); ?>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-12">
                  <div class="form-label-group">
                    <input type="text" id="inputUsername" class="form-control" placeholder="Username" required="required" name="username" autofocus="autofocus">
                    <label for="inputUsername">Username</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" aria-describedby="emailHelp" placeholder="Email address" required="required" name="email">
                <label for="inputEmail">Email address</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="required" name="password_1">
                    <label for="inputPassword">Password</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="confirmPassword" class="form-control" placeholder="Confirm password" required="required" name="password_2">
                    <label for="confirmPassword">Confirm password</label>
                  </div>
                </div>
              </div>
            </div>
            <!--<a class="btn btn-primary btn-block" href="login.html" name="reg_user">Register</a>-->
            <button type="submit" class="btn btn-primary btn-block" name="reg_user">Register</button>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="login.php">Login Page</a>
            <!--<a class="d-block small" href="forgot-password.html">Forgot Password?</a>-->
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    

    <!-- Core plugin JavaScript-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

  </body>

</html>